<a href='https://gitee.com/yhm_my/go-iris'><img src='https://gitee.com/yhm_my/go-iris/widgets/widget_1.svg' alt='go iris web'></img></a>
# go iris RESTful实战

## 目前的界面效果
![gif](https://gitee.com/uploads/images/2019/0407/171832_f98eab28_1537471.gif "a2.gif")
![新增用户](https://images.gitee.com/uploads/images/2019/0401/004110_c620a6fa_1537471.png "屏幕截图.png")
![新增部门](https://gitee.com/uploads/images/2019/0404/172620_c759c129_1537471.png "屏幕截图.png")
![部门table tree](https://gitee.com/uploads/images/2019/0404/172532_abf67191_1537471.png "屏幕截图.png")

### 背景介绍
`感谢iview的作者提供的admin框架，后续一定会支持，哈哈。`
`Golang + Iris(web框架) + Casbin(权限) + JWT + Vue(渐进式js)`的web server框架，可前后端分离。<br />
Iris的教程较少、零散、基础，且框架集合的完整实战案例极少(毕竟多数是用于工作，商业项目)，几乎没有。后期可以直接使用。<br />
源于开源，馈与社区。<br />
称着还有精力在这方面。
***QQ交流群：955576223***

> #### 软件架构
> 目前支持单web架构，如果部署成前后端分离，可用nginx中间件代理(已添加跨域访问设置)。
>    * 采用了Casbin（租户+角色）做Restful的rbac权限控制；
>    * 采用jwt做用户认证、回话控制；
>    * 采用Mysql+xorm做持久层；
>    * admin前端项目持续更新中...，目前在iview-admin分支；

***
#### 项目目录结构
```
go-iris
  +-- conf 所有的配置文件
  +-- doc 说明文档（含go-bindata和mysql脚本文件）
  +-- main
  |------- inits 所有需初始化的目录
  |            +-- bindata 打包后的配置数据
  |            +-- parse 解析配置的golang结构
  |            +-- sys 系统初始化目录
  |            +-- init.go 初始化总入口
  |------- middleware 中间件
  |                 +-- casbin 权限中间件
  |                 +-- jwts JWT中间件
  |                 +-- middleware.go 中间件入口
  |------- utils 工具目录
  |------- web Web分层目录结构
  |          +-- db 数据库工具目录
  |          +-- models 数据模型
  |          +-- routes 路由
  |          +-- supports 一些支持结构
  |          +-- main.go 服务器启动入口文件
  |          +-- main.exe 打包的可执行文件
  +-- resources
  |          +-- static 其他全部静态文件
  |          +-- index.html 网页入口文件
  |          +-- favicon.ico
  +-- build64.bat 打包的脚本文件(windows 64bit)
```

### 使用教程
1. 修改配置：`/conf/db.conf`数据库配置，然后执行`/build.bat`文件，会在`/main/main.exe`生成可执行文件；
2. 直接执行；
3. 如果是mac或其他linux机器，请参考部署说明中的编译，按指定编译就可。

***
#### 部署（不使用nginx情况下），这里在windows 64bit环境下操作为例。依如下步骤操作：
1. **编译server端项目**。在项目下**使用命令行**执行下面的命令(根据你的需要选择目标OS)：
```
[[编译成当前环境]]
go install
[[编译成Linux 64bit]]
set CGO_ENABLED=0
set GOARCH=amd64
set GOOS=linux
go install
[[编译成Mac]]
set CGO_ENABLED=0
set GOARCH=amd64
set GOOS=darwin
go install
编译后的可执行文件在你本地go环境的GOPATH/bin/下找到。
```

2. **启动项目**。执行项目中`/main/main.exe`， ***不需要其他任何依赖文件***。如下图：
![启动](https://images.gitee.com/uploads/images/2019/0401/003202_1f699810_1537471.png "屏幕截图.png")
只要cmd没有退出，说明启动成功

> * 启动后的本地服务地址：localhost:8088<br/>
> * 超级用户登录：
>    > 初始账号：root<br />
>    > 初始密码: 123456
> * 一般用户登录：
>    > 账号：yhm1<br />
>    > 密码：123456


***
> 安装环境
> * golang >= 1.9
> * nginx 不必须
>    > 如果不使用前端环境，直接使用项目下的`/resource/*`的文件，则可以不需要下面的环境：
>    > * vue >= 2.x
>    > * node.js >= v8.9.3（LTS）

> 待需优化项，如：
> * 前端静态文件数据打包
> * 相同密码没随机加密
> * 同一用户生成的token，生成两次前一次没失效
> * 数据库连接池等等....

### QQ交流群：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0314/160120_8c5e3e98_1537471.png "屏幕截图.png")
